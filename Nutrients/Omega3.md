# Nutrient: Omega-3  PolyUnsaturated Fatty Acid (PUFA)

## Contents <a id="contents"></a>
[Summary Notes](#summary-notes-) •
[Omega-3 Sources](#omega-3-sources-) •
[How Much?](#how-much-) •
[Omega-3 & C-Reactive Protein](#omega-3-c-reactive-protein-) •
[Videos](#videos-)

## Summary Notes <a id="summary-notes-"></a><sup>[▴](#contents)</sup>

* Ideally, take a daily baseline of both DHA/EPA (Algae based) and ALA (e.g. Flaxseed) 
    * 250mg to 500mg DHA/EPA Omega-3 Algae based supplement
        * Higher amount (1000 mg to 3000 mg) can be taken for therapeutic healing applications.
    * 1-3 Tablespoon (or "handful") flaxseed provides 2000 mg to 6000 mg ALA Omega-3.
        * either grind flaxseed or add to a 2-minute blended Vitamix smoothie
* Omega-3s are heat sensitive.
    * Store Omega-3 supplements and flaxseeds in refrigerator.
    * Prefer flaxseeds to be raw instead of roasted.
* Benefits of daily Omega-3 intake can take up to 4 to 8 weeks to observe.  Benefits include:
    * reduced stiffness and joint inflammation
    * better mental concentration
    * inflammation reduction


## Omega-3 Sources <a id="omega-3-sources-"></a><sup>[▴](#contents)</sup>

| Supplements | measure | ALA | DHA | EPA | Total Omega-3s |
|--------------------------|:-------:|:-------:|:-----:|:-----:|:-----:|
| [Carlson Labs Cod Liver Oil ⇗][CL]          | 1 teaspoon | -      | 500 mg | 400 mg | 1100mg |
| [Live Wise Algae 3-6-9 ⇗][LW]         | 1 dropper  | 181 mg | 540 mg |  15 mg | 809 mg |
| [Nested Naturals Vegan Omega-3 ⇗][NN] | 1 capsule  | -      | 200 mg | 100 mg | 400 mg |

[CL]:https://carlsonlabs.com/cod-liver-oil-liquid/
[LW]:https://www.livewisenaturals.com/products/algae-omega-3
[NN]:https://nestednaturals.com/supplement/vegan-omega-3-without-the-fish-oil/

| Plant Sources | measure | ALA | DHA | EPA | Total Omega-3s |
|--------------------------|:-------:|:-------:|:-----:|:-----:|:-----:|
| Flaxseeds, whole           | 1 Tablespoon | 2338 mg | - | - | 2338 mg |

* [Algal-oil capsules and cooked salmon: nutritionally equivalent sources of docosahexaenoic acid (DHA) Omega-3 ⇗](https://www.ncbi.nlm.nih.gov/pubmed/18589030)

## How Much? <a id="how-much-"></a><sup>[▴](#contents)</sup>

* Dr. Greger: 1 Tablespoon flaxseeds (ALA) plus 250 mg (EPA/DHA) as a daily baseline 
    * Daily Dozen: [chart ⇗](https://nutritionfacts.org/app/uploads/2018/03/imperial.png), [challenge ⇗](https://nutritionfacts.org/daily-dozen-challenge/), [video ⇗](https://nutritionfacts.org/video/dr-gregers-daily-dozen-checklist/) at least 1 Tablespoon flaxseeds daily
    * [Optimum Nutrition Recommendations ⇗](https://nutritionfacts.org/2011/09/12/dr-gregers-2011-optimum-nutrition-recommendations/) 250 mg daily baseline of pollutant free (yeast- or algae-derived) long-chain omega-3’s (EPA/DHA)
* International Society for the Study of Fatty Acids and Lipids (ISSFAL): [Global Recommendations for EPA and DHA Intake ⇗](https://www.issfal.org/assets/globalrecommendationssummary19nov2014landscape_-3-.pdf) _list recommendations from various international organizations_
    * ISSFAL recommendation: at least 500 mg/day of EPA+DHA for general adult population
    * Multiple recommendations for CardioVascular Disease (CVD) prevention, reduced risk of Coronary Heart Disease (CHD), general cardiovascular health: 500mg/day to 1200mg/day

Observation:

* 1 (300mg) to 2 (600mg) Nested Naturals Vegan Omega-3 provides the a recommended normal daily DHA/EPA baseline.
* 3 (900mg) to 4 (1200mg), or more, Nested Naturals Vegan Omega-3 provides the amount used for therapeutic studies and C-Reactive Protein (CRP) studies.

## Omega-3 & C-Reactive Protein <a id="omega-3-c-reactive-protein-"></a><sup>[▴](#contents)</sup>

The benefit of Omega-3 has been observed by lower [C-Reactive Protein (CRP) ⇗](https://www.mayoclinic.org/tests-procedures/c-reactive-protein-test/about/pac-20385228) in blood test results. DHA/EPA Omega-3 essential fatty acids can help lower C-Reactive Protein levels.
 [[1⇗]](https://academic.oup.com/ndt/article/22/12/3561/1915416), [[2⇗]](https://www.ncbi.nlm.nih.gov/pubmed/23801460)

People with lower C-Reactive Protein levels had significantly higher blood levels of Omega 3 fatty acids. C-Reactive Protein (CRP) is a “response” to inflammation, not a cause of it.

## Videos <a id="videos-"></a><sup>[▴](#contents)</sup>

**Dr. Brooke Goldner: [Fats? Friend or Foe? ⇗](https://www.youtube.com/watch?v=brpsASXF6m8)** (2m47s)

[![Fats? Friend or Foe?](https://img.youtube.com/vi/brpsASXF6m8/mqdefault.jpg "Fats? Friend or Foe?") ](https://www.youtube.com/watch?v=brpsASXF6m8)

**Dr. Greger: [Should We Take EPA & DHA Omega-3 For Our Heart? ⇗](https://nutritionfacts.org/video/should-we-take-epa-and-dha-omega-3-for-our-heart/)** (4m22s)

[![Should Vegans Take DHA to Preserve Brain Function?](https://img.youtube.com/vi/nYAjD6gDS4g/mqdefault.jpg "Should Vegans Take DHA to Preserve Brain Function?") ](https://nutritionfacts.org/video/should-we-take-epa-and-dha-omega-3-for-our-heart/)

* Notes that fish sources of Omega-3s introduce chemicals and toxins which can negate the benefits of Omega-3.

**Dr. Greger Video: [Should Vegans Take DHA to Preserve Brain Function? ⇗](https://nutritionfacts.org/video/should-vegans-take-dha-to-preserve-brain-function/)** (6m6s)

[![Should Vegans Take DHA to Preserve Brain Function?](https://img.youtube.com/vi/h4LvCZ0KnKc/mqdefault.jpg "Should Vegans Take DHA to Preserve Brain Function?") ](https://nutritionfacts.org/video/should-vegans-take-dha-to-preserve-brain-function/)

* A baseline of 250 mg daily of DHA/EPA Omega-3 has been shown to preserve brain volume. 

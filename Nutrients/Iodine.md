# Nutrient: Iodine

<a id="contents"></a>
[Summary Notes](#summary-notes-) •
[Iodine Sources](#iodine-sources-) •
[How Much?](#how-much-) •
[Resources](#resources-)

## Summary Notes <a id="summary-notes-"></a><sup>[▴](#contents)</sup>

## Iodine Sources <a id="-"></a><sup>[▴](#contents)</sup>

Supplements are made of Potassium Iodide or sea vegetables (e.g. kelp) or both.

## How Much? <a id="how-much-"></a><sup>[▴](#contents)</sup>

* RDA: 150 mcg (µg) for adults. 220 mcg (µg) for pregnant women
* 

_Dr. Greger: Optimum Nutrition Recommendations_

> For those who don’t eat seaweed  or use iodized salt, a 150 mcg daily supplement
>
> * The sea vegetable hijiki (hiziki) should not be eaten due to high arsenic levels
> * Kelp should be avoided as it tends to have too much iodine


## Resources <a id="resources-"></a><sup>[▴](#contents)</sup>

* [Dr. Greger: Optimum Nutrition Recommendations ⇗](https://nutritionfacts.org/2011/09/12/dr-gregers-2011-optimum-nutrition-recommendations/)

---

**INBOX**

* <https://www.celticseasalt.com/blog/iodine-the-powerful-nutrient-nearly-everyone-is-missing>
    * <https://www.selinanaturally.com/celtic-sea-salt-gourmet-seaweed-seasoning-2-3-oz-s-cveg>
* <https://realsalt.com/real-salt-iodine-and-radiation/> Redmond Life RealSalt does not add iodine.
    * <https://realsalt.com/comparing-real-salt-to-himalayan-celtic/>
* Himalayan Pink salt may have risk of heavy metals like lead.
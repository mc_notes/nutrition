# Nutrient: Water

## Contents <a id="contents"></a>
[Notes](#notes-) •
[How Much Water?](#how-much-) •
[Volume](#volume-) •
[Resources](#resources-)

## Notes <a id="notes-"></a><sup>[▴](#contents)</sup>

_Water is essential for the majority of chemical reactions in the body._

* A common symptom of dehydration is lack of thirst.

## How Much Water? <a id="how-much-"></a><sup>[▴](#contents)</sup>

* FDA: 8 x 8oz cups = 64 oz water per day
* Dr. Greger: 60 oz water per day (7.5 x 8 oz cups)
* Dr. Goldner: Progress with stall for patients with insufficient water intake. Patients who drink 96 oz (3 quarts or ~ 3 liters) or more water per day have more rapid (non-stalled) healing. Dr. Goldner drank 1 gallon per day during healing (disease reversal) phase.
* Clint Paddison:
    * Even with the water present in food, it is necessary to replenish at least 6 or 7 cups of water per day.
    * > You have nothing to lose and everything to gain by interpreting the pain and inflammation of a rheumatoid joint as a thirst signal in your body and drinking at least 3 liters of water per day.
    * Liquids such as tea, coffee, carbonated drinks and beer all actually cause dehydration.
    * Iraq troops bathroom sign: "To maintain your hydration, ensure you make at least 5 trips to the bathroom a day to expel colorless, odorless urine."

## Volume <a id="volume-"></a><sup>[▴](#contents)</sup>

| Container               | liters | quarts | cups | fl.oz.   |
|-------------------------|:------:|:------:|:----:|:--------:|
| 1 Gallon                |        | 4      | 16   | 128      |
| Mason Jar               |        | 1      | 4    | 32       |
| Orca Coffee Mug         |        | 0.625  |      | 20       |
| Silicon Tumbler         |        | 0.458  | 1.83 | 14.6     |
| Soda Stream             |        | 0.458  | 1.83 | 14.6     |
| Takeya Iced Tea Pitcher |        | 1 or 2 |      | 32 or 64 |
| Vitamix                 | 2      | 2.11   | 8.45 | 67.6     |

## Resources <a id="resources-"></a><sup>[▴](#contents)</sup>
* [Dr. Greger: Daily Dozen Challenge ⬀](https://nutritionfacts.org/daily-dozen-challenge/) [(chart ⬀)](https://nutritionfacts.org/app/uploads/2018/03/imperial.png)

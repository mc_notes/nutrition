# Iron

## Contents <a id="contents"></a>
[Summary Notes](#summary-notes-) •
[Iron Sources](#iron-sources-) •
[How Much?](#how-much-) •
[Resources](#resources-)

## Summary Notes <a id="summary-notes-"></a><sup>[▴](#contents)</sup>

* **heme iron** (animal based, 15-35% typical absorption)
    * body _cannot_ adjust absorption
* **non-heme iron** (plant based, 2-20% typical absorption)
    * body _can_ adjust absorption
* Human body has no specific mechanism to rid itself of excess iron.
* Absorbed in intestines.
* Iron absorption increases when paired with Vitamin C.
* Iron supplements show an increase in oxidative stress.


## Iron Sources <a id="iron-sources-"></a><sup>[▴](#contents)</sup>

**Iron**

* Leafy greens…
    * Spinach: 
        * oxalic acid in spinach (without Vitamin C) prevents more than 90% of iron from being absorbed.
        * young spinach has less oxalic acid than mature spinach
        * 8oz raw (mature?) spinach * 0.8 mg * 0.10 (bioavailable) * 300 ()
* Legumes…
* Nuts…
* Whole Grains…
* Seeds…

**Vitamin C (ascorbic acid)**

* Citrus
* Tropical Fruits
* …

## How Much? <a id="how-much-"></a><sup>[▴](#contents)</sup>

| Category  | US RDA    |
|:---------:|:---------:|
| Female (child bearing age) | 18 mg/day |
| Female (pregnant)          | 27 mg/day |
| Male (19+ years)           | 8 mg/day

_Bioavailability_

* enhancers
    * 1 gram of citric acid was able to increase iron absorption by over 300%
* inhibitors
    * Phytic acid (found in a lot of nuts, grains, and legumes)
    * Tannins and polyphenols (such as found in tea)
    * Calcium
    * Junk foods

_Deficiency_

* Anemia

_Toxicity_

* Iron can be "ferrotoxic". Too much increases risk of colorectal cancer, heart disease, infection, neurodegenerative disorders, and inflammatory conditions.

## Resources <a id="resources-"></a><sup>[▴](#contents)</sup>

* [Dr. Greger: Iron ⇗](https://nutritionfacts.org/topics/iron/)
* PlenteousVeg
    * [Iron and Bioavailability ⇗](https://plenteousveg.com/iron-bioavailability/)
    * [ ⇗](https://plenteousveg.com/spinach-iron/)
* [British Journal of Nutrition: The effects of organic acids, phytates and polyphenols on the absorption of iron from vegetables ⇗](https://www.cambridge.org/core/journals/british-journal-of-nutrition/article/effects-of-organic-acids-phytates-and-polyphenols-on-the-absorption-of-iron-from-vegetables/14A62BC9249CEDBF072AED98BCBA70BB)

# Methods: Green Smoothies

## Contents <a id="contents"></a>
[Summary Notes](#summary-notes-) •
[Videos](#videos-)

## Summary Notes <a id="summary-notes-"></a><sup>[▴](#contents)</sup>

## Videos <a id="videos-"></a><sup>[▴](#contents)</sup>

**Dr. Greger: [Are Green Smoothies Good for You? ⇗](https://nutritionfacts.org/video/are-green-smoothies-good-for-you/)** (4m56s)

[![Are Green Smoothies Good for You?](https://img.youtube.com/vi/6P0BEmtwnZA/mqdefault.jpg "Are Green Smoothies Good for You?") ](https://www.youtube.com/watch?v=6P0BEmtwnZA)


**Dr. Greger: [Are Green Smoothies Bad for You? ⇗](https://nutritionfacts.org/video/are-green-smoothies-bad-for-you/)** (4m42s)

[![Are Green Smoothies Bad for You?](https://img.youtube.com/vi/BARsjJcC8wE/mqdefault.jpg "Are Green Smoothies Bad for You?") ](https://www.youtube.com/watch?v=BARsjJcC8wE)


**Dr. Greger: [Green Smoothies -- What Does the Science Say? ⇗](https://nutritionfacts.org/video/green-smoothies-what-does-the-science-say/)** (4m18s)

[![What Does the Science Say?](https://img.youtube.com/vi/w_tN7NQgtR8/mqdefault.jpg "What Does the Science Say?") ](https://www.youtube.com/watch?v=w_tN7NQgtR8)


**Dr. Greger: [Liquid Calories -- Do Smoothies Lead to Weight Gain? ⇗](https://nutritionfacts.org/video/liquid-calories-do-smoothies-lead-to-weight-gain/)** (8m47s)

[![Do Smoothies Lead to Weight Gain?](https://img.youtube.com/vi/Jz_fhXtmd_c/mqdefault.jpg "Do Smoothies Lead to Weight Gain?") ](https://www.youtube.com/watch?v=Jz_fhXtmd_c)


**Dr. Greger: [Downside of Green Smoothies ⇗](https://nutritionfacts.org/video/the-downside-of-green-smoothies/)** (6m30s)

[![Downside of Green Smoothies](https://img.youtube.com/vi/THlSYDeuIds/mqdefault.jpg "Downside of Green Smoothies") ](https://www.youtube.com/watch?v=THlSYDeuIds)




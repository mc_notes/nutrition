# Harvest: Spices: Cinnamon

Goto: [another page]()

## Contents <a id="contents"></a>
[Section](#section-) •
[Resources](#resources-)


## Section <a id="section-"></a><sup>[▴](#contents)</sup>

* Vietnamese cinnamon
    * coumarin: unknown amount
* Chinese cinnamon (aka Cassia) 
    * coumarin: contains enough `coumarim` to avoid.
* Indonesian
    * coumarin: unknown amount
* Ceylon "True Cinnamon" 
    * coumarin: 

 

## Resources <a id="resources-"></a><sup>[▴](#contents)</sup>

* [NutritionFacts: Cinnamon ⇗](https://nutritionfacts.org/topics/cinnamon/)
    * [2012.04.24 The Safer Cinnamon ⇗](https://nutritionfacts.org/video/the-safer-cinnamon/)
        * Recent data suggests that `coumarin` is toxic to the liver. Use Ceylon.
* Wikipedia
    * [Cinnamon](https://en.wikipedia.org/wiki/Cinnamon)

----
 
**INBOX**

* <https://nutritionfacts.org/topics/cinnamon/>
* <https://nutritionfacts.org/video/update-on-cinnamon-for-blood-sugar-control/>
# Ginger

## Contents <a id="contents"></a>
[How Much?](#how-much-) •
[Resources](#resources-)

## How Much? <a id="how-much-"></a><sup>[▴](#contents)</sup>

* Substitution: 1/2 teaspoon ginger spice ⇄ 1 Tablespoon fresh Ginger
* Reduce menstrual cramps & bleeding: 1/8 teaspoon ginger powder three times daily during one’s period.
* Reduce PMS mood, physical, and behavioral symptoms: 1/8 teaspoon twice a day of ginger power for a week before one’s period. 

## Resources <a id="resources-"></a><sup>[▴](#contents)</sup>

* Dr. Greger
    *  [Fennel Seeds for Menstrual Cramps & PMS ⇗](https://www.w3.org/wiki/Common_HTML_entities_used_for_typography)

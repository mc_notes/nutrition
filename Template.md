# Trial Template

_Document structure evaluation page._

## Contents <a id="contents"></a>
[Section](#section-) •
[Resources](#resources-)

Goto: [here →](), there [<sup>→</sup>](), [next ▸](), [last ▸|](), [somewhere](),

## Section <a id="section-"></a><sup>[▴](#contents)</sup>

## Arrows <a id="section-"></a><sup>[↑](#contents)</sup> <sup>[→](#contents)</sup> <sup>[⇗](#contents)</sup> ↑→⇗  <sup>[▴](#contents)</sup>  <sup>[▸](#contents)</sup> ▴ ▸

## Videos <a id="videos-"></a><sup>[▴](#contents)</sup>

```markdown
[![TITLE](IMAGELINK "TITLE") ](VIDEO_URL)
```

* Video note.

## Resources <a id="resources-"></a><sup>[▴](#contents)</sup>

* [Related: Common HTML Entities ⇗](https://www.w3.org/wiki/Common_HTML_entities_used_for_typography)

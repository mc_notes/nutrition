# Medical: Tests

## Contents <a id="contents"></a>
[Dietary Tests](#dietary-tests-) •
[Medical Tests](#medical-tests-) •
[Notes](#notes-) •
[Resources](#resources-)

## Dietary Tests <a id="dietary-tests-"></a><sup>[▴](#contents)</sup>


**Thyroid**

* Thyroid-Stimulating Hormone (TSH)
* Total Thyroxine (T4)
* Free Thyroxine (T4)
* Free Tri-iodothyronine (T3)

**Vitamin B12**

* **Standard (Serum Cobalamin) B12 Blood Test.** _Not reliable for vegans._
* **Urinary Methylmalonic Acid (MMA µmol/L).** _More reliable. OK for vegans._
* **Holotranscobalamin (HoloTC pmol/L).** _Most reliable (fewer false positives|negatives.)_  

Video: [NutritionFacts: New Vitamin B12 Test ⇗](https://nutritionfacts.org/video/new-vitamin-b12-test/)

**Vitamin D**

## Medical Tests <a id="medical-tests-"></a><sup>[▴](#contents)</sup>

**Antinuclear Antibodies (ANA) Screen**

**C-Reactive Protein (CRP)** 

A measure of systemic inflamation.

Paddison program recommends monthly CRP testings.

**Immunoglobulins IgA, IgG, IgM**

**Sedimentation Rate (ESR)** Request monthly.

A nonspecific test used to detect chronic inflammation associated with infections, autoimmune disorders, and cancer.

Paddison program recommends monthly ESR testings.

## Notes <a id="notes-"></a><sup>[▴](#contents)</sup>

* Monthly blood results provide feedback on healing progress.

* Even if you 'feel better' soon, it is very difficult to quantify any progress without an initial reference point in your bloodwork.

* Ensure that your doctor requests on your blood test forms to have a copy sent to you every time you get your blood tested.

## Resources <a id="resources-"></a><sup>[▴](#contents)</sup>

* [HealthLabs ⇗](https://www.healthlabs.com/)
* [LabCorp ⇗](https://www.labcorp.com/)
* [Life Extension: Lab Tests ⇗](https://www.lifeextension.com/Vitamins-Supplements/Blood-Tests/Blood-Tests)
    * [Autoimmune Disease Screen](https://www.lifeextension.com/vitamins-supplements/itemlc100041/autoimmune-disease-screen)
